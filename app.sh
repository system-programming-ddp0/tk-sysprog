#!/bin/bash

function all_menu(){
  date
  echo "======================="
  echo "       Main Menu       "
  echo "======================="
  echo "1. Change parameter"
  echo "2. Print mice input"
  echo "3. Exit"
}

function pause(){
  read -p "Press [Enter] key to continue..." readEnterKey
}

function read_choice(){
  local c
  read -p "choose 1-3: " c
  case $c in
    1) param ;;
    2) mice ;;
    3) echo "Bye Bye...."; exit 0 ;;
    *) echo "Please choose between 1-3"
  esac
}

function header(){
  local h="$@"
  echo "-------------------------------------------------------"
  echo "              ${h}              "
  echo "-------------------------------------------------------"
}

function param(){
  header "mouse0 parameter"
  ./change_psmouse_rate.sh
  echo "psmouse rate parameter changed into 200"
  echo "check /sys/module/psmouse/parameters and read rate file to see the changes"
  pause
}

function mice(){
  header "mice input"
  timeout 10s ./midTier2.sh
  echo "dev mice input printed to hasil.txt"
  pause
}

while true
do
  all_menu
  read_choice
done

