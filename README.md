# TK Sysprog Kelompok DDP0

Kelas: A

Anggota kelompok:
1. Dinda Inas Putri
2. Inez Amandha Suci
3. Siti Khadijah

Link Drive: [Google Drive TK Sysprog Kel DDP0](https://drive.google.com/drive/folders/1fVgpT7SzCGDJ-Swb7dQGOKuQh-NBC69j?usp=sharing)

Catatan:
File config-4.15.1ddp0-version adalah file config setelah meremove beberapa modul, nama asli filenya adalah config-4.15.1deb-ddp0-version, tetapi di-rename untuk dipush di gitlab agar tidak me-replace file config yang satunya. Untuk menjalankannya, perlu mengubah namanya kembali menjadi config-4.15.1deb-ddp0-version.

Untuk menjalankan program, jalankan:
- ./app.sh
- ./change_psmouse_rate.sh
- ./change_video_brightness.sh
- ./lowTier2.sh
- ./midTier2.sh

